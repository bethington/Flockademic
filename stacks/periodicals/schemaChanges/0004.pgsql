---------------------------------------------------------
--                      WARNING                        --
-- Do not modify this script after merging it to       --
-- `dev`. Only add new, consecutively numbered         --
-- changesets in case the schema needs further         --
-- changes.                                            --
---------------------------------------------------------

-- NOTE: When running locally. Docker Compose won't pick up changes in SQL files unless you recreate the volume:
--       docker-compose rm -v; docker-compose up --build;

BEGIN;
  ALTER TABLE scholarly_articles ADD COLUMN date_created TIMESTAMP;

  UPDATE scholarly_articles
  SET date_created=author.date_created
  FROM scholarly_article_authors author
  WHERE scholarly_articles.identifier=author.scholarly_article;

  ALTER TABLE scholarly_articles ALTER COLUMN date_created SET NOT NULL;
  ALTER TABLE scholarly_articles ALTER COLUMN date_created SET DEFAULT CURRENT_TIMESTAMP;
COMMIT;
