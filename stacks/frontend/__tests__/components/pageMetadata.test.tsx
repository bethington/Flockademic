jest.mock('react-ga');

import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

import { ScholarlyArticle } from '../../../../lib/interfaces/ScholarlyArticle';
import { PageMetadata } from '../../src/components/pageMetadata/component';

it('should render a title even if none was provided', () => {
  const quip = shallow(<PageMetadata url="https://arbitrary_url"/>);

  expect(quip.find('title')).toExist();
});

it('should render all OpenGraph data', () => {
  const quip = shallow((
    <PageMetadata
      url="https://some_url"
      title="Some title"
      description="Some description"
      image={{ alt: 'arbitrary alt text', url: 'some_url' }}
    />
  ));

  expect(quip.find('meta[name="og:type"]').prop('content')).toBe('website');
  expect(quip.find('meta[name="og:url"]').prop('content')).toBe('https://some_url');
  expect(quip.find('meta[name="og:title"]').prop('content')).toMatch('Some title');
  expect(quip.find('meta[name="og:description"]').prop('content')).toBe('Some description');
  expect(quip.find('meta[name="og:image"]').prop('content')).toBe('some_url');
});

it('should render all Twitter Cards data', () => {
  const quip = shallow((
    <PageMetadata
      url="https://some_url"
      title="Some title"
      description="Some description"
      image={{ alt: 'Some alt text', url: 'some_url' }}
    />
  ));

  expect(quip.find('meta[name="twitter:card"]').prop('content')).toBe('summary_large_image');
  expect(quip.find('meta[name="twitter:site"]').prop('content')).toBe('@Flockademic');
  expect(quip.find('meta[name="twitter:title"]').prop('content')).toMatch('Some title');
  expect(quip.find('meta[name="twitter:description"]').prop('content')).toBe('Some description');
  expect(quip.find('meta[name="twitter:image"]').prop('content')).toBe('some_url');
  expect(quip.find('meta[name="twitter:image:alt"]').prop('content')).toBe('Some alt text');
});

it('should update the Twitter card type when no image is set', () => {
  const quip = shallow((
    <PageMetadata
      url="https://some_url"
    />
  ));

  expect(quip.find('meta[name="twitter:card"]').prop('content')).toBe('summary');
});

it('should render all Google Scholar/Microsoft Academic Search bibliography data', () => {
  const article: Partial<ScholarlyArticle> = {
    author: [ { identifier: 'Arbitrary author ID', name: 'Some author' } ],
    datePublished: '2002-02-14T13:37:00.000Z',
    identifier: 'some-identifier',
    isPartOf: { identifier: 'Arbitrary journal ID', name: 'Some journal name' },
    name: 'Some name',
  };

  const quip = shallow((
    <PageMetadata
      url="https://some_url"
      article={article}
    />
  ));

  expect(quip.find('meta[name="citation_title"]').prop('content')).toBe('Some name');
  expect(quip.find('meta[name="citation_author"]').prop('content')).toBe('Some author');
  expect(quip.find('meta[name="citation_publication_date"]').prop('content')).toMatch('2002/2/14');
  expect(quip.find('meta[name="citation_journal_title"]').prop('content')).toBe('Some journal name');
  expect(quip.find('meta[name="citation_pdf_url"]').prop('content')).toMatch('?download');
});

it('should render no bibliography data when an unconfigured article was provided', () => {
  const article: Partial<ScholarlyArticle> = {
  };

  const quip = shallow((
    <PageMetadata
      url="https://some_url"
      article={article}
    />
  ));

  expect(quip.find('meta[name="citation_title"]')).not.toExist();
  expect(quip.find('meta[name="citation_author"]')).not.toExist();
  expect(quip.find('meta[name="citation_publication_date"]')).not.toExist();
  expect(quip.find('meta[name="citation_journal_title"]')).not.toExist();
  expect(quip.find('meta[name="citation_pdf_url"]')).not.toExist();
});
