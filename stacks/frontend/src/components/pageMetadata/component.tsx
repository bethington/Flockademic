import { DateTime } from 'luxon';
import * as React from 'react';
import { Helmet } from 'react-helmet';

import { ScholarlyArticle } from '../../../../../lib/interfaces/ScholarlyArticle';
import { getPageTitle } from '../../services/getPageTitle';

interface PageMetadataProps {
  url: string;
  article?: Partial<ScholarlyArticle>;
  description?: string;
  image?: { alt: string; url: string };
  title?: string;
}

export function PageMetadata(props: PageMetadataProps) {
  return (
    <Helmet>
      {renderUrl(props.url)}
      {renderTitle(props.title)}
      {renderDescription(props.description)}
      {renderImage(props.image)}
      {renderBibliographicData(props.article)}
      <meta name="og:type" content="website"/>
      <meta name="twitter:card" content={props.image ? 'summary_large_image' : 'summary'}/>
      <meta name="twitter:site" content="@Flockademic"/>
    </Helmet>
  );
}

function renderUrl(url: string) {
  /* istanbul ignore if: document.location is not set for tests */
  if (url.charAt(0) === '/') {
    url = `https://${document.location.host}${url}`;
  }

  return <meta key="meta_url_1" name="og:url" content={url}/>;
}

function renderTitle(title?: string) {
  const fullTitle = getPageTitle(title);

  // react-helmet doesn't support fragments yet, so return an array:
  // https://github.com/nfl/react-helmet/issues/342
  return [
    <title key="meta_title_1">{fullTitle}</title>,
    <meta key="meta_title_2"name="og:title" content={fullTitle}/>,
    <meta key="meta_title_3"name="twitter:title" content={fullTitle}/>,
  ];
}

function renderDescription(description?: string) {
  if (!description) {
    return null;
  }

  // react-helmet doesn't support fragments yet, so return an array:
  // https://github.com/nfl/react-helmet/issues/342
  return [
    <meta key="meta_description_1" name="description" content={description}/>,
    <meta key="meta_description_2" name="og:description" content={description}/>,
    <meta key="meta_description_3" name="twitter:description" content={description}/>,
  ];
}

function renderImage(image?: { alt: string; url: string }) {
  if (!image) {
    return null;
  }

  // react-helmet doesn't support fragments yet, so return an array:
  // https://github.com/nfl/react-helmet/issues/342
  return [
    <meta key="meta_image_1" name="og:image" content={image.url}/>,
    <meta key="meta_image_2" name="twitter:image" content={image.url}/>,
    <meta key="meta_image_3" name="twitter:image:alt" content={image.alt}/>,
  ];
}

// For details on the format, see https://scholar.google.com/intl/en/scholar/inclusion.html#indexing
function renderBibliographicData(article?: Partial<ScholarlyArticle>) {
  if (!article) {
    return null;
  }

  const bibliographicData: JSX.Element[] = [];

  if (article.name) {
    bibliographicData.push(<meta key="meta_citation_title" name="citation_title" content={article.name}/>);
  }

  if (article.author) {
    article.author
    .map((author, index) => (
      <meta
        key={`meta_citation_author_${index}`}
        name="citation_author"
        content={author.name}
      />
    ))
    .forEach((tag) => bibliographicData.push(tag));
  }

  if (article.datePublished) {
    const date = DateTime.fromISO(article.datePublished).toUTC();
    const formattedDate = `${date.year}/${date.month}/${date.day}`;
    bibliographicData.push(
      <meta key="meta_citation_publication_date" name="citation_publication_date" content={formattedDate}/>,
    );
  }

  if (article.isPartOf && article.isPartOf.name) {
    bibliographicData.push(
      <meta key="meta_citation_journal_title" name="citation_journal_title" content={article.isPartOf.name}/>,
    );
  }

  if (article.identifier) {
    // We're not using article.associatedMedia[0].contentUrl here,
    // because Google Scholar requires the PDF to be in the same subdirectory:
    // https://scholar.google.com/intl/en/scholar/inclusion.html#indexing
    bibliographicData.push(
      <meta
        key="meta_citation_pdf_url"
        name="citation_pdf_url"
        content={`https://${document.location.host}/article/${article.identifier}?download`}
      />,
    );
  }

  return bibliographicData;
}
